# README #

This is a PHP Engineer Test called Superheros for Tienda Nube. Is developed in Laravel Framework version 5.4.36 with Bootstrap. 

Please, follow the next steps to install the aplication:

1 - If you already have installed PHP and MySQL you can skip this step. If not, download and install Laragon from https://laragon.org/download/ 

2 - Download the proyect in www folder.

3 - Change the database configuration in the .env file in project root.

4 - Create tables importing superheroes.sql file in project root. Also you may run the migrations with the next command: php artisan migrate.
