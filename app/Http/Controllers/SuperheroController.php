<?php
namespace App\Http\Controllers;

use App\PictureLogic;
use App\Superhero;
use App\SuperheroLogic;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class SuperheroController extends Controller
{
    public function index() {
        return view('superhero.index', [
            'superheros' => Superhero::orderBy('nickname')->get()
        ]);
    }
    
    public function index_list() {
        return view('superhero.list', [
            'superheros' => Superhero::orderBy('nickname')->get(),
        ]);
    }
    
    public function show($id = null)
    {
        $superhero = isset($id)? Superhero::find($id): new Superhero();
        return view('superhero.show', [
            'id'=>$id, 
            'superhero'=>$superhero,
            'pictures'=>$superhero->pictures()
        ]);
    }
    
    public function store(Request $request) 
    {
        if($request->ajax())
        {
            $validator = Validator::make($request->all(), 
                ['nickname'=>'required'],
                ['required'=>'Required']
            );

            if(!$validator->passes()) {
                return response()->json(['error'=>$validator->errors()]);
            }
            
            $id = $request->get('id');
            $superhero = empty($id)? new Superhero(): Superhero::find($id);
            $superhero->fill([
                'nickname' => $request->nickname,
                'real_name' => $request->real_name,
                'origin_description' => $request->origin_description,
                'superpowers' => $request->superpowers,
                'catch_phrase' => $request->catch_phrase
            ]);
            $superhero->save();
            
            $arrayPictureId = $request->get('picture_id');
            $arrayFile = $request->file('file');

            foreach ($arrayPictureId as $i => $id) {
                if(!empty($arrayPictureId[$i]) && !empty($arrayFile[$i])) {
                    PictureLogic::store($id, $arrayFile[$i], $superhero->id);
                }
            }
            return response()->json(['id'=>$superhero->id], 200);
        }
    }
    
    public function destroy(Request $request, $id) 
    {
        if($request->ajax())
        {
            SuperheroLogic::destroy($id);
            return response()->json(['id'=>$id], 200);
        }
    }
}