<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Picture extends Model
{
    protected $table = 'picture';
    
    public function superheroe() {
        return $this->hasOne('App\Superhero', 'id', 'superhero')->first();
    }
}
