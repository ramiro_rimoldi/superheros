<?php
namespace App;

use App\Picture;

class PictureLogic
{
    public static function destroy($id) 
    {
        $picture = Picture::find($id);
        if(file_exists(public_path().'/files/'.$picture->name)) 
        {
            unlink(public_path().'/files/'.$picture->name);
            $picture->delete();
        }
    }
    
    public static function store($id, $file, $superheroId) 
    {
        if($file->isValid()) 
        {
            $picture = ($id == 'new')? new Picture(): Picture::find($id);                    
            $name = date('YmdHis').'_'.$file->getClientOriginalName();
            $file->move(public_path("files"), $name);
            $picture->name = $name;
            $picture->superhero = $superheroId;
            $picture->save();
        }
    }
}
