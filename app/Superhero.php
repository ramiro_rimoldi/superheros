<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Superhero extends Model
{
    protected $table = 'superhero';
    protected $fillable = ['nickname','real_name','origin_description','superpowers','catch_phrase'];
    
    public function pictures() {
        return $this->hasMany('App\Picture', 'superhero', 'id')->get();
    }
}
