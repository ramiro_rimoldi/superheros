<?php
namespace App;

use App\Superhero;
use App\PictureLogic;

class SuperheroLogic
{
    public static function destroy($id) 
    {
        $superhero = Superhero::find($id);
        $pictures = $superhero->pictures();
        foreach($pictures as $one) {
            PictureLogic::destroy($one->id);
        }
        $superhero->delete();
    }
}
