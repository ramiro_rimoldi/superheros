<?php

use Faker\Generator as Faker;

$factory->define(App\Superhero::class, function (Faker $faker) {
    return [
        'nickname' => $faker->name,
        'real_name' => $faker->name,
        'origin_description' => $faker->text,
        'superpowers' => $faker->text,
        'catch_phrase' => $faker->text
    ];
});