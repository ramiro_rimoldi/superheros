$(document).ready(function(){
    
    smartTable();
    var getUrl = window.location;
    var baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];
    
    $('.container.superhero').on('click', '.new-superhero', function() {
        $.blockUI({ message:'' });   
        $( ".container.superhero" ).load( baseUrl + "/show", function() {
            $.unblockUI();
        });
    });
    
    $('.container.superhero').on('click', '.edit', function() {
        $.blockUI({ message:'' });  
        $( ".container.superhero" ).load( baseUrl + "/show/" + $(this).data('id'), function() {
            $.unblockUI();
        });
    });
    
    $('.container.superhero').on('click', '.destroy', function() {
        $.blockUI({ message:'' });   
        $(this).parent().parent().hide();
        $.get(baseUrl + "/destroy/" + $(this).data('id'), function() {
            $.unblockUI();
        });
        return false;
    });
    
    $('.container.superhero').on('click', '.add-picture', function(){
        var newOption = $( ".one-row-form" ).clone();
        newOption.removeClass("one-row-form").removeClass("hidden").appendTo(".list-editor tbody");
        return false;
    });
    
    $('.container.superhero').on('click', '.back', function() {
        $.blockUI({ message:'' });   
        $( ".container.superhero" ).load( baseUrl + "/list", function() {
            smartTable();
            $.unblockUI();
        });
        return false;
    });
    
    $('.container.superhero').on('click', '.store', function(e) {
        e.preventDefault();
        $.blockUI({ message:'' }); 
        $('.alert-danger').html('');
        var formData = new FormData($(this).parents('form')[0]);

        $.ajax({
            url: baseUrl + '/show',
            type: 'POST',
            xhr: function() {
                var myXhr = $.ajaxSettings.xhr();
                return myXhr;
            },
            data: formData,
            cache: false,
            contentType: false,
            processData: false
        }).done(function( data ) {
            if(!$.isEmptyObject(data.error)) {
                $('.panel-heading.success').addClass('hidden');
                printErrorMsg(data.error);
            } else {
                $('input[name=id]').val(data.id);
                $('.panel-heading.success').removeClass('hidden');
            }
            $.unblockUI();
        });
        return false;
    });
    
    $('.container.superhero').on('click', '.destroy-picture', function() {
        $.blockUI({ message:'' });   
        $(this).parent().parent().hide();
        $.get(baseUrl + "/destroy_picture/" + $(this).data('id'), function() {
            $.unblockUI();
        });
        return false;
    });
});

function smartTable() 
{
    $('#dataTables').DataTable({
        "responsive": true,
        "lengthMenu": [ 5, 10, 15 ],
        "columns": [
          null,
          { "orderable": false },
          { "orderable": false },
          { "orderable": false }
        ]
    });
}

function printErrorMsg (msg) 
{
    $(".alert-danger").html('');
    $.each( msg, function( key, value ) {
        $(".alert-danger."+key).html(value);
    });
}