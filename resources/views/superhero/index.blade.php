@extends('layouts.master', ['asset'=>'superhero'])

@section('title', 'Superheros')

@section('content')            
    @include('superhero.list')
@stop