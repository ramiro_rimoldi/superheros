<div class="form-group">
    <label>{{$label}}</label>
    <input class="form-control" name="{{$name}}" type="text" value="{{$value}}">
    <span class="alert-danger {{$name}}"></span>
</div>