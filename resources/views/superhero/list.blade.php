<div class="row">
    <div class="col-lg-12">
        <h3 class="page-header">Superheros</h3>
    </div>
</div>
<div class="row">
    <div class="col-lg-8 col-lg-offset-2">
        <div class="panel panel-default">
            <div class="panel-heading">
                <button class="btn btn-primary new-superhero">+ New</button>
            </div>
            <div class="panel-body">
                <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables">
                    <thead>
                        <tr>
                            <th>Superhero</th>
                            <th></th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($superheros as $one)
                        <tr>
                            <td>{{$one->nickname}}</td>
                            <td>
                                <?php $pictures = $one->pictures() ?>
                                @if(count($pictures) > 0)
                                    <img class="picture-list" src="{{asset('files/'.$pictures[0]->name)}}" />
                                @endif
                            </td>
                            <td>
                                <button class="btn btn-success edit" data-id="{{$one->id}}">Update</button>
                            </td>
                            <td>
                                <a class="btn btn-danger destroy" data-id="{{$one->id}}">Delete</a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>