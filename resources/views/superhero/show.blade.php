<div class="row">
    <div class="col-lg-12">
        <h3 class="page-header">Superhero Editor</h3>
    </div>
</div>
<form role="form" method="POST" autocomplete="off" enctype="multipart/form-data">
    <div class="row">
        <div class="col-lg-8 col-lg-offset-2">
            <div class="panel panel-primary">
                <div class="panel-heading success hidden">
                    <h4>Superhero Saved.</h4>
                </div>
                <div class="panel-heading">
                    <button class="btn btn-success back">Back</button>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-10 col-lg-offset-1">
                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                            <input type="hidden" name="id" value="{{$superhero->id}}">
                            @include('superhero.input_text',[
                                'label' => 'Nickname',
                                'name' => 'nickname',
                                'value' => $superhero->nickname
                            ])
                            @include('superhero.input_text',[
                                'label' => 'Real Name',
                                'name' => 'real_name',
                                'value' => $superhero->real_name
                            ])
                            @include('superhero.textarea',[
                                'label' => 'Origin Description',
                                'name' => 'origin_description',
                                'value' => $superhero->origin_description
                            ])
                            @include('superhero.textarea',[
                                'label' => 'Superpowers',
                                'name' => 'superpowers',
                                'value' => $superhero->superpowers
                            ])
                            @include('superhero.textarea',[
                                'label' => 'Catch Phrase',
                                'name' => 'catch_phrase',
                                'value' => $superhero->catch_phrase
                            ])
                        </div>
                        <div class="col-lg-10 col-lg-offset-1">
                            <div class="form-group list-editor">
                                <table width="100%" class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>Pictures</th>
                                            <th></th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($pictures as $one)
                                            <tr>
                                                <input type="hidden" name="picture_id[]" value="{{$one->id}}">
                                                <td>
                                                    <input name="file[]" type="file" />
                                                </td>
                                                <td>
                                                    <img class="picture-list" src="{{asset('files/'.$one->name)}}" />
                                                </td>
                                                <td>
                                                    <button class="btn btn-danger destroy-picture" data-id="{{$one->id}}">Delete</button>
                                                </td>
                                            </tr>
                                        @endforeach
                                        <tr>
                                            <input type="hidden" name="picture_id[]" value="new">
                                            <td>
                                                <input name="file[]" type="file" />
                                            </td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <tr class="one-row-form hidden">
                                            <input type="hidden" name="picture_id[]" value="new">
                                            <td>
                                                <input name="file[]" type="file" />
                                            </td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                    </tbody>
                                </table>
                                <div class="row">
                                    <div class="col-lg-12 center">
                                        <button class="btn btn-info add-picture">+ Add Picture</button>
                                        <button class="btn btn-primary store">Save</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>    
</form>