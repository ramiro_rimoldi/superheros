<div class="form-group">
    <label>{{$label}}</label>
    <textarea class="form-control" name="{{$name}}">{{$value}}</textarea>
    <span class="alert-danger {{$name}}"></span>
</div>