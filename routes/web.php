<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'SuperheroController@index');
Route::get('list', 'SuperheroController@index_list');
Route::get('show/{id?}', 'SuperheroController@show');
Route::post('show/{id?}', 'SuperheroController@store');
Route::get('destroy/{id}', 'SuperheroController@destroy');
Route::get('destroy_picture/{id}', 'SuperheroController@destroyPicture');

