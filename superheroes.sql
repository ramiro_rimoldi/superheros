-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1:3306
-- Tiempo de generación: 11-05-2018 a las 00:40:13
-- Versión del servidor: 5.7.19
-- Versión de PHP: 7.0.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `superheroes`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2018_05_10_172452_create_superhero_table', 1),
(2, '2018_05_10_173101_create_picture_table', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `picture`
--

DROP TABLE IF EXISTS `picture`;
CREATE TABLE IF NOT EXISTS `picture` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `superhero` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `picture_superhero_foreign` (`superhero`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `picture`
--

INSERT INTO `picture` (`id`, `superhero`, `name`, `created_at`, `updated_at`) VALUES
(4, 7, '20180510224750_latest[1]', '2018-05-11 01:47:50', '2018-05-11 01:47:50'),
(5, 8, '20180510225019_aHR0cDovL3d3dy5uZXdzYXJhbWEuY29tL2ltYWdlcy9pLzAwMC8wOTYvMzU4L29yaWdpbmFsL3N1cGVybWFucmVldmVfMDIuanBn[1]', '2018-05-11 01:50:19', '2018-05-11 01:50:19'),
(6, 8, '20180510225058_superman-liga-de-la-justicia-1[1].jpg', '2018-05-11 01:50:58', '2018-05-11 01:50:58');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `superhero`
--

DROP TABLE IF EXISTS `superhero`;
CREATE TABLE IF NOT EXISTS `superhero` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nickname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `real_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `origin_description` text COLLATE utf8mb4_unicode_ci,
  `superpowers` text COLLATE utf8mb4_unicode_ci,
  `catch_phrase` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `superhero`
--

INSERT INTO `superhero` (`id`, `nickname`, `real_name`, `origin_description`, `superpowers`, `catch_phrase`, `created_at`, `updated_at`) VALUES
(7, 'Batman', 'Bruce Wayne', 'Le mataron a los padres y proteje a ciudad gotica', 'Tiene plata', 'Catch Phrase', '2018-05-10 23:33:13', '2018-05-11 03:12:51'),
(8, 'Superman', 'Clark Kent', 'viene de otro planeta', 'muchos', NULL, '2018-05-11 01:50:19', '2018-05-11 01:50:19');

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `picture`
--
ALTER TABLE `picture`
  ADD CONSTRAINT `picture_superhero_foreign` FOREIGN KEY (`superhero`) REFERENCES `superhero` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
