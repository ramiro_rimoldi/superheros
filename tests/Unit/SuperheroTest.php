<?php

namespace Tests\Unit;

use App\Superhero;
use Tests\TestCase;
use Faker\Factory;

class SuperheroTest extends TestCase
{
    public function testShowSuperhero()
    {
        $superhero = factory(\App\Superhero::class)->create();
        $found = Superhero::find($superhero->id);
        $this->assertInstanceOf(Superhero::class, $found);
        $this->assertEquals($found->nickname, $superhero->nickname);
        $this->assertEquals($found->real_name, $superhero->real_name);
        $this->assertEquals($found->origin_description, $superhero->origin_description);
        $this->assertEquals($found->superpowers, $superhero->superpowers);
        $this->assertEquals($found->catch_phrase, $superhero->catch_phrase);
        $superhero->delete();
    }
    
    public function testCreateSuperhero()
    {
        $faker = Factory::create();
        $data = [
            'nickname' => $faker->name,
            'real_name' => $faker->name,
            'origin_description' => $faker->text,
            'superpowers' => $faker->text,
            'catch_phrase' => $faker->text
        ];
        
        $superhero = new Superhero();
        $superhero->fill($data);
        
        $this->assertInstanceOf(Superhero::class, $superhero);
        $this->assertEquals($data['nickname'], $superhero->nickname);
        $this->assertEquals($data['real_name'], $superhero->real_name);
        $this->assertEquals($data['origin_description'], $superhero->origin_description);
        $this->assertEquals($data['superpowers'], $superhero->superpowers);
        $this->assertEquals($data['catch_phrase'], $superhero->catch_phrase);
    }
    
    public function testEditSuperhero()
    {
        $superhero = factory(\App\Superhero::class)->make();
        $faker = Factory::create();
        $data = [
            'nickname' => $faker->name,
            'real_name' => $faker->name,
            'origin_description' => $faker->text,
            'superpowers' => $faker->text,
            'catch_phrase' => $faker->text
        ];
        $superhero->fill($data);
        
        $this->assertInstanceOf(Superhero::class, $superhero);
        $this->assertEquals($data['nickname'], $superhero->nickname);
        $this->assertEquals($data['real_name'], $superhero->real_name);
        $this->assertEquals($data['origin_description'], $superhero->origin_description);
        $this->assertEquals($data['superpowers'], $superhero->superpowers);
        $this->assertEquals($data['catch_phrase'], $superhero->catch_phrase);
    }
    
    public function testDeleteSuperhero()
    {
        $superhero = factory(\App\Superhero::class)->create();
        $this->assertTrue($superhero->delete());
    }
    
    public function testNavigation()
    {
        $responseHome = $this->get('/');
        $responseHome->assertStatus(200);
        $responseHome->assertSee('Superheros');
        
        $responseEditor = $this->get('/show');
        $responseEditor->assertStatus(200);
        $responseEditor->assertSee('Superhero Editor');
        
        $responsePostEditor = $this->json('POST', '/show');
        $responsePostEditor->assertStatus(200);
    }
}
